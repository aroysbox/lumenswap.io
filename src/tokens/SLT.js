import logo from 'src/assets/images/slt.png';

export default {
  code: 'SLT',
  logo,
  web: 'smartlands.io',
  issuer: 'GCKA6K5PCQ6PNF5RQBF7PQDJWRHO6UOGFMRLK3DYHDOI244V47XKQ4GP',
};
