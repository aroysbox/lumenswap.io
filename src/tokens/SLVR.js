import logo from 'src/assets/images/slvr.png';

export default {
  code: 'SLVR',
  logo,
  web: 'www.mintx.com',
  issuer: 'GBZVELEQD3WBN3R3VAG64HVBDOZ76ZL6QPLSFGKWPFED33Q3234NSLVR',
};
