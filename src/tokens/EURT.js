import logo from 'src/assets/images/eurt.png';

export default {
  code: 'EURT',
  logo,
  web: 'k.tempocrypto.com',
  issuer: 'GAP5LETOV6YIE62YAM56STDANPRDO7ZFDBGSNHJQIYGGKSMOZAHOOS2S',
};
