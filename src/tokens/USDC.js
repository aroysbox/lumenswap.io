import logo from 'src/assets/images/usdc.png';

export default {
  code: 'USDC',
  logo,
  web: 'circle.com',
  issuer: 'GA5ZSEJYB37JRC5AVCIA5MOP4RHTM335X2KGX3IHOJAPP5RE34K4KZVN',
};
