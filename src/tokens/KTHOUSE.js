import logo from 'src/assets/images/kthouse.png';

export default {
  code: 'KTHOUSE',
  logo,
  web: 'kthouse.co',
  issuer: 'GB5BK7HJK7SJGJXN4X6BV2QBEWGXVMEZJRGKWBFU6PK6BV4DW7WOQ27W',
};

