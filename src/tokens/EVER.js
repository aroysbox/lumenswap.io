import logo from 'src/assets/images/everlife.png';

export default {
  code: 'EVER',
  logo,
  web: 'everlife.ai',
  issuer: 'GDRCJ5OJTTIL4VUQZ52PCZYAUINEH2CUSP5NC2R6D6WQ47JBLG6DF5TE',
};
