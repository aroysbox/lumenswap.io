import logo from 'src/assets/images/xlm-logo.png';

export default {
  code: 'XLM',
  issuer: 'native',
  web: 'stellar.org',
  logo,
};
